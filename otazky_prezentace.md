# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** |7 hodin|
| jak se mi to podařilo rozplánovat |2 intervaly|
| návrh designu | [https://gitlab.spseplzen.cz/radad/dopravnisituace/-/tree/main/dokumentace/fotky] |
| proč jsem zvolil tento design |jednoduchost|
| zapojení | [https://gitlab.spseplzen.cz/radad/dopravnisituace/-/tree/main/dokumentace/fotky] |
| z jakých součástí se zapojení skládá |kabely, ledky|
| realizace | [https://gitlab.spseplzen.cz/radad/dopravnisituace/-/tree/main/dokumentace/fotky] |
| jaký materiál jsem použil a proč |Lego, mám jo moc|
| co se mi povedlo |vše|
| co se mi nepovedlo/příště bych udělal/a jinak |ne Lego|
| zhodnocení celé tvorby |2|