int red = 1;
int yellow = 2;
int green = 3;
int red2 = 4;
int yellow2 = 5;
int green2 = 6;
int redP = 8;
int greenP = 9;
int redP2 = 10;
int greenP2 = 11;
int button = 12;
int fotorezistor = A0;
bool tlacitkoStisk = false;
bool tlacitkoPraveStisk = false;
unsigned long tlacitkoNestisknute = 0;

void setup() {
  pinMode(red, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(red2, OUTPUT);
  pinMode(yellow2, OUTPUT);
  pinMode(green2, OUTPUT);
  pinMode(redP, OUTPUT);
  pinMode(greenP, OUTPUT);
  pinMode(redP2, OUTPUT);
  pinMode(greenP2, OUTPUT);
  pinMode(button, INPUT_PULLUP);
}

void loop() {
  int svetlo = analogRead(fotorezistor);

  if (digitalRead(button) == LOW && !tlacitkoStisk) {
    tlacitkoStisk = true;
    tlacitkoPraveStisk = true;
    tlacitkoNestisknute = millis();
  }

  if (tlacitkoStisk) {
    unsigned long elapsedTime = millis() - tlacitkoNestisknute;
    if (elapsedTime < 5000) {
      digitalWrite(greenP, HIGH);
      digitalWrite(redP, LOW);
      digitalWrite(greenP2, HIGH);
      digitalWrite(redP2, LOW);
      digitalWrite(yellow2, LOW);
      digitalWrite(yellow, LOW);
      digitalWrite(red, HIGH);
      digitalWrite(red2, HIGH);
    } else {
      digitalWrite(greenP, LOW);
      digitalWrite(redP, HIGH);
      digitalWrite(greenP2, LOW);
      digitalWrite(redP2, HIGH);
      tlacitkoStisk = false;
    }
  }

  if (svetlo < 100) {
    digitalWrite(green, LOW);
    digitalWrite(green2, LOW);
    digitalWrite(red, LOW);
    digitalWrite(red2, LOW);
    digitalWrite(yellow, HIGH);
    digitalWrite(yellow2, HIGH);
    delay(500);
    digitalWrite(yellow, LOW);
    digitalWrite(yellow2, LOW);
    delay(500);
  } else {
    if (!tlacitkoPraveStisk) {
      digitalWrite(yellow2, LOW);
      digitalWrite(red, HIGH);
      digitalWrite(red2, HIGH);
      delay(1000);

      digitalWrite(red, LOW);
      digitalWrite(green, HIGH);
      digitalWrite(red2, HIGH);
      digitalWrite(green2, LOW);
      digitalWrite(yellow, LOW);
      digitalWrite(yellow2, LOW);
      delay(1000);

      digitalWrite(red2, HIGH);
      digitalWrite(yellow, HIGH);
      digitalWrite(green, LOW);
      digitalWrite(yellow2, HIGH);
      delay(1000);

      digitalWrite(yellow, LOW);
      digitalWrite(yellow2, LOW);
      digitalWrite(green2, HIGH);
      digitalWrite(red2, LOW);
      digitalWrite(green, LOW);
      digitalWrite(red, HIGH);
      delay(1000);

      digitalWrite(green2, LOW);
      digitalWrite(green, LOW);
      digitalWrite(yellow, HIGH);
      digitalWrite(yellow2, HIGH);
      digitalWrite(red2, LOW);
      delay(1000);
    }
    if (tlacitkoPraveStisk && millis() - tlacitkoNestisknute >= 5000) {
      tlacitkoPraveStisk = false;
    }
  }
}
